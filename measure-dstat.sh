#!/bin/bash

CHECK_JOB_DELAY=2
LOG_FILE="raw-cpu-stats-dstat.log"

# check arguments
if [ $# -eq 0 ]
then
    echo "Usage: $0 <command-to-execute --with --args>"
    exit 1;
fi;
DSTAT_PATH="`dirname \"$0\"`"   # find where dstat lives

# print job command
echo -e "(i) Supplied job command:\n\n    $@\n" >&2

# init log file
echo -n "" > $LOG_FILE
sleep 1

# start dstat measurement
$DSTAT_PATH/dstat --cpu-use -t --noheaders --cpufreq >> $LOG_FILE &
dstatPID=$!

echo "(i) Started dstat on background (PID: $dstatPID)" >&2

# sleep 10s
echo "(i) Sleeping 10s to stabilize before starting the job ..." >&2
sleep 10

# start the job
"$@" &
jobPID=$!

# check whether it is still running
while [ ! -z "$( ps -ef | grep $jobPID | grep -v grep )" ]
do
    sleep $CHECK_JOB_DELAY
done

# job finished, kill dstat
kill $dstatPID

echo "(i) Job ended (PID: $jobPID), dstat killed." >&2
