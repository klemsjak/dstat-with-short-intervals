#!/bin/bash

DELTA_T=0.1
CHECK_JOB_DELAY=2
LOG_FILE="raw-cpu-stats-top.log"

# check arguments
if [ $# -eq 0 ]
then
    echo "Usage: $0 <command-to-execute --with --args>"
    exit 1;
fi;

# print job command
echo -e "(i) Supplied job command:\n\n    $@\n" >&2

# init log file
echo "# T-STAMP PID   USER  PR NI VIRT  RES  SHR  S %CPU %MEM TIME+  COMMAND" > $LOG_FILE
sleep 1

# start the job
"$@" &
jobPID=$!
echo "# Job PID: $jobPID" >> $LOG_FILE

# start top measurement
# n.b.: command > >(grep foo) &   # redirects to new bash, keeping PID of command in $!
#~ top -b -d $DELTA_T -p $jobPID > >(grep --line-buffered '^\s*[0-9]' | while read line; do d=`date +%M:%S.%3N`; echo $d $line; done >> $LOG_FILE) &
top -H -b -d $DELTA_T -p $jobPID > >( \
while read line ; do
    if grep -q '^top' <<< $line ; then
        echo "TS: `date +%M:%S.%3N`"
    elif grep -q '^\s*[0-9]' <<< $line ; then   # \b[SR]\b
        echo $line
    fi
done >> $LOG_FILE) &

topPID=$!

# check whether it is still running
while [ ! -z "$( ps -ef | grep $jobPID | grep -v grep | grep -v top )" ]
do
    sleep $CHECK_JOB_DELAY
done

# job finished, kill top
kill $topPID

echo "(i) Job ended (PID: $jobPID), top killed." >&2



# top - 11:56:38 up 46 min,  1 user,  load average: 0,29, 0,30, 0,35
# Tasks:   1 total,   1 running,   0 sleeping,   0 stopped,   0 zombie
# %Cpu(s):  4,8 us, 24,1 sy,  0,0 ni, 68,7 id,  0,0 wa,  0,0 hi,  2,4 si,  0,0 st
# MiB Mem :   7612,4 total,   1247,1 free,   3661,1 used,   2704,2 buff/cache
# MiB Swap:   1425,7 total,   1425,7 free,      0,0 used.   3021,8 avail Mem
#
#     PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
#    7870 fakub     20   0  411084  69984   3484 R  93,8   0,9   0:02.48 bench-parmesan



# top - 12:03:22 up 52 min,  1 user,  load average: 0,78, 0,58, 0,47
# Threads:   5 total,   4 running,   1 sleeping,   0 stopped,   0 zombie
# %Cpu(s): 97,5 us,  2,5 sy,  0,0 ni,  0,0 id,  0,0 wa,  0,0 hi,  0,0 si,  0,0 st
# MiB Mem :   7612,4 total,   1197,0 free,   3712,1 used,   2703,2 buff/cache
# MiB Swap:   1425,7 total,   1425,7 free,      0,0 used.   2986,6 avail Mem
#
#     PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
#    8362 fakub     20   0  425408 150956   5396 R  99,9   1,9   0:00.29 bench-parmesan
#    8364 fakub     20   0  425408 150956   5396 R  95,0   1,9   0:00.28 bench-parmesan
#    8365 fakub     20   0  425408 150956   5396 R  95,0   1,9   0:00.28 bench-parmesan
#    8363 fakub     20   0  425408 150956   5396 R  90,0   1,9   0:00.27 bench-parmesan
#    8358 fakub     20   0  425408 150956   5396 S   0,0   1,9   0:06.23 bench-parmesan
